<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


$app->post('api/user', [
  'middleware' => 'cors', 'uses' => 'UserController@create'
]);

$app->put('checkIn', [
    'as' => 'checkIn', 'uses' => 'UserController@checkIn'
]);

$app->put('checkOut', [
    'as' => 'checkOut', 'uses' => 'UserController@checkOut'
]);

// Route::get('example', array('middleware' => 'cors', 'uses' => 'ExampleController@dummy'));
$app->get('api/getRooms', [
    'middleware' => 'cors', 'uses' => 'RoomController@getRooms'
]);

$app->get('/', function () use ($app) {
    // return '<html><head><script src="https://code.jquery.com/jquery-3.2.1.min.js"></script></head></html>';
    return File::get(public_path() . '/pwa/index.html');
});
