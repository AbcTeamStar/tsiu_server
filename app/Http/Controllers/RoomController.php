<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RoomController extends Controller
{
    public function getRooms(){
      // $rooms = DB::select('select * from room');
      $rooms = DB::select('SELECT id, name, (SELECT COUNT(*)
                                             FROM stay
                                             WHERE time_out is NULL AND id = id_room) as people
                           FROM room');
      return response()->json([
            'body' => $rooms,
            'error' => false
      ]);
    }
}
