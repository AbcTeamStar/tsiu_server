<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    /**
     * Retrieve the user for the given ID.
     *
     * @param  int  $id
     * @return Response
     */
    public function create(Request $request){
        $data = $request->json()->all();
        $name = $data['name'];
        $sex = $data['sex'];
        $uuid = uniqid();
        DB::insert('insert into user (uuid, name, sex) values (?, ?, ?)', [$uuid, $name, $sex]);

        $profile = [
          'name' => $name,
          'sex'  => $sex,
          'uuid' => $uuid
        ];

        return response()->json([
              'body' => $profile,
              'error' => false
        ]);
    }

    public function checkIn(Request $request){
        $uuid = $request->input('uuid');
        $room = $request->input('room');
        $id_user = $this->getUserId($uuid);
        DB::insert('insert into stay (id_user, id_room) values (?, ?)', [$id_user, $room]);

        return response()->json([
              'body' => "Success: check-in room " . $room,
              'error' => false
        ]);
    }

    public function checkOut(Request $request){
        $uuid = $request->input('uuid');
        $current_timestamp = date('Y-m-d H:i:s');
        $id_user = $this->getUserId($uuid);
        $affected = DB::update('update stay set time_out = ? where id_user = ?', [$current_timestamp, $id_user]);

        return response()->json([
              'body' => "Success: check-out " . $current_timestamp,
              'error' => false
        ]);
    }

    private function getUserId($uuid){
      $user = DB::select('select id from user where uuid = ?', [$uuid]);
      return $user[0]->id;
    }
}
